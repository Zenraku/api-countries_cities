const mysql   = require('mysql');
const dbConfig = require("./db.config.js");

//Connection BDD
var mySqlClient = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

mySqlClient.connect(function(err) {
  if (err) throw err;
  console.log("Database Connected!");
});
  

module.exports = mySqlClient;