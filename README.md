# API-Countries_Cities

API de données Villes/Départements/Pays au niveau mondial pour projets personnels.
<br/>
Dernière mise à jour des données de l'API 08/01/2019

## Pour commencer

L'accès à la documentation peut se faire aussi via www.pmaury.fr/api/world_list/docs.<br/>
<br/>
Toutes les réponses sont au format JSON.<br/>
<br/>
Pour toutes les requêtes (sauf la documentation), une clé est nécessaire : /api/world_list/{request}?key={MyKey}
<br>
--> Liste de tous les pays : /api/world_list/countries?key={MyKey}<br/>
--> Liste de tous les états/départements d'un pays : /api/world_list/states?key={MyKey}&country={IdCountry}<br/>
--> Liste de toutes les villes d'un état/département : /api/world_list/cities?key={MyKey}&state={IdStates}<br/>

### Pré-requis
Backend :
- [express](https://www.npmjs.com/package/express)
- [mysql](https://www.npmjs.com/package/mysql)<br>
+ **TODO** Migrer vers mongodb

## Démarrage

- npm start (Uniquement en DEV).

## Fabriqué avec

* [Node.Js](https://nodejs.org/) - Plateforme Javascript (back-end)
* [Express.Js](https://expressjs.com/fr/) - Framework Javascript (back-end)
* [Visual Studio Code](https://code.visualstudio.com/) - Editeur de textes


## Versions

**Dernière version stable :** 1.0<br/>
**Dernière version :** 1.0<br/>
Liste des versions : <br/>[Cliquer pour afficher](https://gitlab.com/Zenraku/api-countries_cities/-/tags)

## Auteurs

* **Pierre Maury** _alias_ [@Zenraku](https://github.com/Zenraku)