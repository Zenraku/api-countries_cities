//Imports
const express = require('express');
const bodyParser = require("body-parser");
const sql = require("./lib/db/db.js");
const validate = require("./lib/functions/validate.js");

//Instantiate server 
const app = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//Configure routes

app.get('/api/world_list/countries', validate.validateQuery(['key']), (req, res) => {
    
    const sqlCountries = 'SELECT * FROM `countries`';
    validate.validateKey(req.query.key, function(validate) {
        if(validate == true) { 
            sql.query(
                sqlCountries,
                function results(error, results) {
                    if(error) {
                        res.status(500).send('Erreur SQL, can\'t get countries ');
                    } else {
                        res.json(results);
                        res.status(200);
                    }
                }
            );
        } else {
            res.status(500).send('Erreur SQL, can\t verify the key');
        }
    });
});

app.get('/api/world_list/states', validate.validateQuery(['key', 'country']), (req, res) => {
    const sqlStates = 'SELECT * FROM `states` WHERE `country_id` = '+ req.query.country;
    validate.validateKey(req.query.key, function(validate) {
        if(validate == true) { 
            sql.query(
                sqlStates,
                function results(error, results) {
                    if(error) {
                        res.status(500).send('Erreur SQL, can\'t get states ');
                    } else {
                        res.json(results);
                        res.status(200);
                    }
                }
            );
        } else {
            res.status(500).send('Erreur SQL, can\t verify the key');
        }
    });
});

app.get('/api/world_list/cities', validate.validateQuery(['key', 'state']), (req, res) => {
    const sqlCities = 'SELECT * FROM `cities` WHERE `state_id` = '+ req.query.state;
    validate.validateKey(req.query.key, function(validate) {
        if(validate == true) { 
            sql.query(
                sqlCities,
                function results(error, results) {
                    if(error) {
                        res.status(500).send('Erreur SQL, can\'t get cities ');
                    } else {
                        res.json(results);
                        res.status(200);
                    }
                }
            );
        } else {
            res.status(500).send('Erreur SQL, can\t verify the key');
        }
    });
});

app.get('/api/world_list/docs', (req, res) => {
    const docs = {
        maj: "Dernière mise à jour 10/12/2019",
        docs: "Accès à la documentation via /api/world_list/docs",
        json: "Toutes les réponses sont au format JSON, parce que.",
        key: "Key d'accès nécessaire pour toute requête (sauf docs) /api/world_list/{request}?key={MyKey}",
        countries: "Liste de tout les pays /api/world_list/countries?key={MyKey}",
        states: "Liste des états/départements d'un pays /api/world_list/states?key={MyKey}&country={IdCountry}",
        cities: "Liste des villes d'un états/départements /api/world_list/cities?key={MyKey}&state={IdStates}"
    }
    res.json(docs);
    res.status(200);
});

//Launch server 
app.listen(8080, function() {
    console.log('Server en cours')
});